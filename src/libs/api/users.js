import axios from 'axios'
import API from '../api'
var base64 = require('base-64');


const create = function (data) {
  let param = {
    userName: data.userName,
    email: data.email,
    password: base64.encode(JSON.stringify(data.password))
  };
  
  // console.log('data '+JSON.stringify(param));
  // return axios.post(API.createUser, data).then(response => {
  //   return response
  // })
  return axios.post(`/register`, param).then(response => {
    return response
  })
}

export { create }
