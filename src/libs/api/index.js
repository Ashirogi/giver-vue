import * as Users from './users';

// import API from '../api';
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:2000';

axios.interceptors.request.use(function (config) {
  if (localStorage.getItem('token')) {
    const token = localStorage.getItem('token');
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
  }
  return config;
})

axios.interceptors.response.use(
  function (response) {
    return response
  },
  function (error) {
    if (error.response.status === 401) {
      localStorage.removeItem('token');
      this.$router.push('/pages/login');
    }
    return Promise.reject(error);
  }
)

export {
  Users,
}
